package mymapper;

import mypo.Student;

public interface StudentMapper {
    //查询
    public Student findStudentBySno(int sno) throws Exception;

    //增加
    public void insertStudent(Student student) throws Exception;

    //修改
    public void updateStudent(Student student) throws Exception;

    //删除
    public void deleteStudent(int sno) throws Exception;


}
