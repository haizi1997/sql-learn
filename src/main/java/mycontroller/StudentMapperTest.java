package mycontroller;

import java.io.InputStream;
import mymapper.StudentMapper;
import mypo.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;



public class StudentMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void setUp() throws Exception {

        String resource="SqlMapConfig.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        //创建会话工厂,传入mybatis的配置文件信息
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    //查找用户信息
    @Test
    public void testFindStudentBySno() throws Exception {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建UserMapper的对象
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

        //调用userMapper的方法
        Student student = studentMapper.findStudentBySno(110);

        sqlSession.close();

        System.out.println(student);
    }

    //添加用户信息
    @Test
    public void insertStudentTest() throws Exception{

        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建UserMapper的对象
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

        //插入数据
        Student student = new Student();
        student.setSno(111);
        student.setName("李四");
        student.setAge(18);

        //调用userMapper的方法
        studentMapper.insertStudent(student);

        //提交事务
        sqlSession.commit();

        sqlSession.close();
    }

    //修改用户信息
    @Test
    public void updateStudentTest() throws Exception {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建UserMapper的对象
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

        //插入数据
        Student student = new Student();
        student.setSno(111);
        student.setAge(16);

        //调用userMapper的方法
        studentMapper.updateStudent(student);

        //提交事务
        sqlSession.commit();

        sqlSession.close();
    }

    //删除用户信息
    @Test
    public void deleteStudentTest() throws Exception {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建UserMapper的对象
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

        //调用userMapper的方法
        studentMapper.deleteStudent(111);

        //提交事务
        sqlSession.commit();

        sqlSession.close();
    }

}
